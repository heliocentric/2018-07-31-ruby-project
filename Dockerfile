FROM ruby:2.4
COPY . /usr/app
RUN cd /usr/app && bundle install
ENTRYPOINT sh -c 'cd /usr/app && bundle exec ruby myapp.rb'
